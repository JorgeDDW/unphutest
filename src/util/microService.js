const micro = require('micro')
const {json} = require('micro')
const {validateJwt} = require('../db/util/validators')

class MicroService {
  constructor(options) {
    this.options = options
  }

  async createMethod(method, req, res, callback) {
    let params = null
    let headers = req.headers

    if (method === "GET") {
      params = req.params
    }else {
      params = await json(req)
    }
    try {
      //here i can do authentication stuff for all requests
      let s = await validateJwt(headers.authorization)
      if (s.auth === true) {
        let result = await callback({params, headers})

        micro.send(res, 200, result)
      }else {
        micro.send(res, 404, {error: 'invalid jwt token'})
      }
    } catch (e) {
      console.log(e)
      // 404 istead of throw new error
      micro.send(res, 404, {error: 'error'})
    }
  }

  async createMethodNoAuth(method, req, res, callback) {
    let params = null
    let headers = req.headers

    if (method === "GET") {
      params = req.params
    }else {
      params = await json(req)
    }
    try {
      //no auth for this method
      let result = await callback({params, headers})

      micro.send(res, 200, result)
    } catch (e) {
      console.log(e)
      // 404 istead of throw new error
      micro.send(res, 404, {error: 'error'})
    }
  }
}

module.exports = MicroService
