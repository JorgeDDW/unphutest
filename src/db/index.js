const Sequelize = require('sequelize')
const Op = Sequelize.Op
const db = require('./db')

module.exports = {
  db,
  Op
}

//see this url to use operators https://sequelize.org/master/manual/querying.html
