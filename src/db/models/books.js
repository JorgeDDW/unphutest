const {setupSequelize} = require('../setUpDb')
const Sequelize = require('sequelize')

module.exports = function (config) {
  let sequelize = setupSequelize(config);

  return sequelize.define('books', {
      title: {
        type: Sequelize.STRING
      },
      description: {
        type: Sequelize.STRING
      },
      publicationDate: {
        type: Sequelize.DATE
      },
      author: {
        type: Sequelize.STRING
      },
      userId: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        type: Sequelize.DATE
      },
      updatedAt: {
        type: Sequelize.DATE
      }
    },{
      tableName:'books',
      timestamps: false
    }
  )
}
