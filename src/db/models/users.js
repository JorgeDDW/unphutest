const {setupSequelize} = require('../setUpDb')
const Sequelize = require('sequelize')

module.exports = function (config) {
  let sequelize = setupSequelize(config);

  return sequelize.define('users', {
      name: {
        type: Sequelize.STRING
      },
      lastName: {
        type: Sequelize.STRING
      },
      email: {
        type: Sequelize.STRING
      },
      createdAt: {
        type: Sequelize.DATE
      },
      updatedAt: {
        type: Sequelize.DATE
      }
    },{
      tableName:'users',
      timestamps: false
    }
  )
}
