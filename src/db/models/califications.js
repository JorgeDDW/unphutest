const {setupSequelize} = require('../setUpDb')
const Sequelize = require('sequelize')

module.exports = function (config) {
  let sequelize = setupSequelize(config);

  return sequelize.define('califications', {
      vote: {
        type: Sequelize.INTEGER
      },
      bookId: {
        type: Sequelize.INTEGER
      },
      userId: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        type: Sequelize.DATE
      },
      updatedAt: {
        type: Sequelize.DATE
      }
    },{
      tableName:'califications',
      timestamps: false
    }
  )
}
