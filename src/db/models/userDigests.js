const {setupSequelize} = require('../setUpDb')
const Sequelize = require('sequelize')

module.exports = function (config) {
  let sequelize = setupSequelize(config);

  return sequelize.define('userDigests', {
      passwordDigest: {
        type: Sequelize.STRING
      },
      userId: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        type: Sequelize.DATE
      },
      isActive: {
        type: Sequelize.BOOLEAN
      }
    },{
      tableName:'userDigests',
      timestamps: false
    }
  )
}
