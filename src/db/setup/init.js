const Sequelize = require('sequelize')
const {config} = require('../config')
const conf = config({env: process.env.USER})
const models = require('./sql')
const chalk = require('chalk');

//it uses same config
let db = new Sequelize(conf.dbConfig)

/*
  You have to create the db manually
    `CREATE DATABASE unphutest;`
*/

async function createModels() {
  for (let m of models) {
    try {
        let result = await db.query(String(m))
        console.log(chalk.green(`Created ${m}`))
    } catch (e) {
      if(e.message.match(/already exists/)){
        console.log(chalk.red(e.message))
      }
    }
  }
}

async function setup(){
  try {
    await createModels()
    process.exit(0)
  } catch (e) {
    process.exit(0)
  }
}

setup()
