const jwt = require('jsonwebtoken')
const {findSessionHash} = require('../../util/storeManager')
const secretKey = process.env.SECRET_KEY_TOKEN || 'xDstriker_:D'

async function validateJwt(token) {
  try {
    if (token) {
      //token contais just a property: hash
        const decoded = await jwt.verify(token, `${secretKey}`)
        let {hash} = decoded
        let result = findSessionHash(hash)
        if (result) {
          return result
        }else {
          return {auth: false}
        }
    }else {
      return `invalid token`
    }
  } catch (e) {
    console.log(e)
    return `invalid token`
  }
}

function createJwtToken(params, expiresIn = '3', timeToExpire = 'd') {
    return jwt.sign(params, `${secretKey}`, { expiresIn: `${expiresIn}${timeToExpire}` })
}

module.exports = {
  validateJwt,
  createJwtToken
}
