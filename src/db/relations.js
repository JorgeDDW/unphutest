const {
  books,
  califications,
  users,
  userDigests
} = require('./models')

module.exports = function (config) {
  const Book = books(config)
  const Calification = califications(config)
  const User = users(config)
  const UserDigest = userDigests(config)

  /* Here goes all the realtions */

    Book.hasMany(Calification)
    Calification.belongsTo(Book)

    User.hasMany(Book)
    Book.belongsTo(User)

    User.hasMany(UserDigest)
    UserDigest.belongsTo(User)

  /* End */

  return {
    Book,
    Calification,
    User,
    UserDigest
  }
}
/*
  This can be performed using sequelize import
  and letting sequelize putting all together.
*/
